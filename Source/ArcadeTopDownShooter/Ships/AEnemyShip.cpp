// Fill out your copyright notice in the Description page of Project Settings.


#include "AEnemyShip.h"

// Sets default values
AAEnemyShip::AAEnemyShip()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AAEnemyShip::BeginPlay()
{
	Super::BeginPlay();
	
}

void AAEnemyShip::SpawnProjectile()
{
	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParams.Instigator = this;

	ABaseProjectile* SpawnedActor = GetWorld()->SpawnActor<ABaseProjectile>(ProjectileClass, GetActorLocation(), GetActorRotation(), SpawnParams);
	MoveIgnoreActorAdd(SpawnedActor);

}
// Called every frame
void AAEnemyShip::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AAEnemyShip::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

